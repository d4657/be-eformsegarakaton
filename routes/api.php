<?php

use App\Http\Controllers\API\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('/profile', 'UserController@getProfile');
    Route::post('/logout', 'UserController@logout');

    Route::get('/users/admin', 'UserController@getAllAdmin');
    Route::get('/users/editor', 'UserController@getAllEditor');
    Route::get('/users/staff', 'UserController@getAllStaff');
    Route::get('/users/penduduk', 'UserController@getAllMasyarakat');

    Route::get('/staffs', 'UserController@findStaffByName');

    Route::post('/users-e-sign', 'UsersEsignController@createUsersEsign');
    Route::get('/users-e-sign', 'UsersEsignController@getAllUsersEsign');
    Route::get('/users-e-sign/{id}', 'UsersEsignController@getUsersEsignById');
    Route::put('/users-e-sign/{id}', 'UsersEsignController@updateUserEsignById');
    Route::get('/users-e-sign/user/{id}', 'UsersEsignController@getAllUsersEsignByUserId');
    Route::put('/users-e-sign/acc/{id}', 'UsersEsignController@acceptEsign');

    Route::delete('/users-e-sign/{id}', 'UsersEsignController@delUsersEsignById');
    Route::get('/surat', 'SuratController@getSurat');
    Route::get('/surat/{id}', 'SuratController@getSuratById');
});


Route::middleware(['auth:sanctum', 'type.admin'])->prefix('admin')->group(function () {
    // user endpoint
    Route::get('/profile', 'UserController@getProfile');
    Route::put('/profile/bio', 'UserController@updateBio');
    Route::put('/profile/account', 'UserController@updateAccount');
    Route::put('/profile/password', 'UserController@updatePassword');
    Route::post('/profile/image', 'UserController@updateUserImages');

    Route::get('/user', 'UserController@getAllUser');
    Route::get('/user/{id}', 'UserController@getUserById');
    Route::put('/user/bio/{id}', 'UserController@updateBioById');
    Route::put('/user/account/{id}', 'UserController@updateAccountById');
    Route::put('/user/password/{id}', 'UserController@updatePasswordById');
    Route::post('/user/image/{id}', 'UserController@updateUserImagesById');
    Route::put('/user/upgrade/{id}', 'UserController@setUserLevel');
    Route::delete('/user/{id}', 'UserController@deleteUserById');

    // surat
    Route::post('/surat', 'SuratController@createSurat');


    Route::put('/surat/{id}', 'SuratController@updateSuratById');
    Route::post('/surat/template/{id}', 'SuratController@updateTemplateById');
    Route::delete('/surat/{id}', 'SuratController@deleteSuratById');

    // global access endpoint
    Route::post('/e-sign', 'ESignController@createEsign');
    Route::get('/e-sign', 'ESignController@getAllEsign');
    Route::get('/e-sign/{id}', 'ESignController@getEsignById');
    Route::put('/e-sign/{id}', 'ESignController@updateEsignById');
    Route::delete('/e-sign/{id}', 'ESignController@deleteEsignById');
    Route::put('/e-sign/acc/{id}', 'ESignController@acceptEsignById');

    // global access endpoint
    Route::post('/e-form', 'EFormController@createEform');
    Route::get('/e-form', 'EFormController@getAllEform');
    Route::get('/e-form/{id}', 'EFormController@getEformById');
    Route::put('/e-form/{id}', 'EFormController@updateEformById');
    Route::post('/e-form/doc/{id}', 'EFormController@updateEformDocById');
    Route::delete('/e-form/{id}', 'EFormController@deleteEformById');
    Route::put('/e-form/acc/{id}', 'EFormController@acceptEformById');
});

Route::middleware(['auth:sanctum', 'type.editor'])->prefix('editor')->group(function () {
    Route::get('/profile', 'UserController@getProfile');
    Route::put('/profile/bio', 'UserController@updateBio');
    Route::put('/profile/account', 'UserController@updateAccount');
    Route::put('/profile/password', 'UserController@updatePassword');
    Route::post('/profile/image', 'UserController@updateUserImages');


    Route::get('/user', 'UserController@getAllUser');
    Route::get('/user/{id}', 'UserController@getUserById');
    Route::put('/user/bio/{id}', 'UserController@updateBioById');


    Route::post('/surat', 'SuratController@createSurat');


    Route::put('/surat/{id}', 'SuratController@updateSuratById');
    Route::delete('/surat/{id}', 'SuratController@deleteSuratById');

    Route::post('/e-sign', 'ESignController@createEsign');
    Route::get('/e-sign', 'ESignController@getAllEsign');
    Route::get('/e-sign/{id}', 'ESignController@getEsignById');
    Route::put('/e-sign/{id}', 'ESignController@updateEsignById');
    Route::put('/e-sign/acc', 'ESignController@batchAcceptEsign');
    Route::put('/e-sign/acc/{id}', 'ESignController@acceptEsignById');
    Route::delete('/e-sign', 'ESignController@batchDeleteEsign');
    Route::delete('/e-sign/{id}', 'ESignController@deleteEsignById');

    Route::post('/e-form', 'EFormController@createEform');
    Route::get('/e-form', 'EFormController@getAllEform');
    Route::get('/e-form/{id}', 'EFormController@getEformById');
    Route::put('/e-form/{id}', 'EFormController@updateEformById');
    Route::put('/e-form/acc', 'EFormController@batchAcceptEform');
    Route::put('/e-form/acc/{id}', 'EFormController@acceptEformById');
    Route::delete('/e-form', 'EFormController@batchDeleteEform');
    Route::delete('/e-form/{id}', 'EFormController@deleteEformById');
});

Route::middleware(['auth:sanctum', 'type.staff'])->prefix('staff')->group(function () {
    Route::get('/profile', 'UserController@getProfile');
    Route::put('/profile/bio', 'UserController@updateBio');
    Route::put('/profile/account', 'UserController@updateAccount');
    Route::put('/profile/password', 'UserController@updatePassword');
    Route::post('/profile/image', 'UserController@updateUserImages');

    Route::get('/user', 'UserController@getAllUser');
    Route::get('/user/{id}', 'UserController@getUserById');

    Route::post('/e-sign', 'ESignController@createEsign');
    Route::get('/e-sign', 'ESignController@getAllEsign');
    Route::get('/e-sign/{id}', 'ESignController@getEsignById');
    Route::put('/e-sign/{id}', 'ESignController@updateEsignById');
    Route::put('/e-sign/acc', 'ESignController@batchAcceptEsign');
    Route::put('/e-sign/acc/{id}', 'ESignController@acceptEsignById');

    Route::post('/e-form', 'EFormController@createEform');
    Route::get('/e-form', 'EFormController@getAllEform');
    Route::get('/e-form/{id}', 'EFormController@getEformById');
    Route::put('/e-form/{id}', 'EFormController@updateEformById');
    Route::put('/e-form/acc', 'EFormController@batchAcceptEform');
    Route::put('/e-form/acc/{id}', 'EFormController@acceptEformById');
});

Route::middleware(['auth:sanctum', 'type.penduduk'])->prefix('penduduk')->group(function () {
    Route::get('/profile', 'UserController@getProfile');
    Route::put('/profile/bio', 'UserController@updateBio');
    Route::put('/profile/account', 'UserController@updateAccount');
    Route::put('/profile/password', 'UserController@updatePassword');
    Route::post('/profile/image', 'UserController@updateUserImages');

    Route::get('/user', 'UserController@getAllUser');
    Route::get('/user/{id}', 'UserController@getUserById');

    Route::post('/e-sign', 'ESignController@createEsign');
    Route::get('/e-sign', 'ESignController@getAllEsign');
    Route::get('/e-sign/{id}', 'ESignController@getEsignById');
    Route::put('/e-sign/{id}', 'ESignController@updateEsignById');

    Route::post('/e-form', 'EFormController@createEform');
    Route::get('/e-form', 'EFormController@getAllEform');
    Route::get('/e-form/{id}', 'EFormController@getEformById');
    Route::put('/e-form/{id}', 'EFormController@updateEformById');
});

//unprotected route
Route::post('/register', 'UserController@register');
Route::post('/login', 'UserController@login');

// downloadable file
Route::get('/storage/foto/{filename}', 'FileController@getUserFoto');
Route::get('/storage/tandatangan/{filename}', 'FileController@getUserTandatangan');
Route::get('/storage/e-sign/qr/{id}', 'FileController@getQrEsign');
Route::get('/storage/e-sign/qr-sign/{id}', 'FileController@getQrAndSignEsign');
Route::get('/storage/template/{id}', 'FileController@getTemplate');
// Route::get('/storage/temp/{filename}');

Route::get('/accepted-esign/{id}', 'UsersEsignController@getAcceptedEsign');
