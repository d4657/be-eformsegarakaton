<?php

namespace App\Helpers;

/**
 * Format Response
*/
class ResponseFormatter
{
  /**
   * API Response
   *
   * @var array
  */
  protected static $response = [
    'meta' => [
      'code' => 200,
      'status' => 'success',
      'message' => null
    ],
    'payload' => null,
  ];

  /**
   * Give Successful Response
  */
  public static function success($payload = null, $message = null){
    self::$response['meta']['message'] = $message;
    self::$response['payload'] = $payload;

    return response()->json(self::$response, self::$response['meta']['code']);
  }

  /**
   * Give Error Response
  */
  public static function error($errors = null, $message = null, $code = 400){
    self::$response['meta']['status'] = 'error';
    self::$response['meta']['code'] = $code;
    self::$response['meta']['message'] = $message;
    self::$response['errors'] = $errors;

    return response()->json(self::$response, self::$response['meta']['code']);
  }
}
