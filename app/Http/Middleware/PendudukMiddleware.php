<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class PendudukMiddleware
{
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle(Request $request, Closure $next)
  {
    if (auth()->user()->tokenCan('role:penduduk')) {
      return $next($request);
    }

    return response()->json('Penduduk Not Authorized', 401);
  }
}
