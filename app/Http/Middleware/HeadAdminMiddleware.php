<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class HeadAdminMiddleware
{
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle(Request $request, Closure $next)
  {
    if (auth()->user()->tokenCan('role:head-admin')) {
      return $next($request);
    }

    return response()->json('Head Admin Not Authorized', 401);
  }
}
