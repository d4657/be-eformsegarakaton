<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\ESign;
use App\Models\Surat;
use Illuminate\Support\Facades\Response;
use Intervention\Image\ImageManagerStatic as Image;
use SebastianBergmann\Template\Template;

class FileController extends Controller
{
    public function getUserFoto($filename)
    {
        $path = public_path() . '/storage/foto/' . $filename;
        return Response::download($path);
    }
    public function getUserTandatangan($filename)
    {
        $path = public_path() . '/storage/tandatangan/' . $filename;
        return Response::download($path);
    }

    public function getQrEsign($id)
    {
        $path = public_path() . '/storage/esign-acc/' . $id . '.png';
        return Response::download($path);
    }

    public function getQrAndSignEsign($id)
    {
        $path = public_path() . '/storage/esign-acc/' . $id . '.png';
        $path2 = public_path() . '/storage/tandatangan/' . ESign::find($id)->penerima->tandatangan;

        $canvas = Image::canvas(950, 320);
        $canvas->insert(Image::make($path), 'top-left', 10, 10);
        $canvas->insert(Image::make($path2), 'top-left', 340, 10);

        return  $canvas->response('png', 100);
    }

    public function getTemplate($id)
    {
        $surat = Surat::find($id);
        $path = public_path() . '/storage/template/' . $surat->template;
        return Response::download($path);
    }

    // public function getQrAndSignForEsign($id ){

    //     $path = public_path().'/storage/tandatangan/'.$id.'.png';
    //     return Response::download($path);
    // }
}
