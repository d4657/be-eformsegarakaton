<?php

namespace App\Http\Controllers\API;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Models\ESign;
use App\Models\User;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ESignController extends Controller
{
    public function createEsign(Request $request)
    {
        $request->validate([
            'judul' => ['required'],
            'id_penerima' => ['required'],
            'perihal' => ['required'],
            'keperluan_pribadi' => ['boolean'],
            'waktu_valid' => ['date']
        ]);

        $esign = ESign::create([
            'judul' => $request->judul,
            'sender_id' => Auth::user()->id,
            'id_penerima' => $request->id_penerima,
            'perihal' => $request->perihal,
            'waktu_valid' => $request->waktu_valid
        ]);

        if ($request->keperluan_pribadi == true) {
            $esign->waktu_diterima = new DateTime();
            $esign->catatan_tambahan = $request->catatan_tambahan;
            $esign->id_penerima = Auth::user()->id;
        }

        $esign->save();
        return ResponseFormatter::success($esign, 'e-form berhasil diajukan');
    }

    public function getAllEsign(Request $request)
    {
        $esign = ESign::all();
        return ResponseFormatter::success($esign, 'semua esign  berhasil diambil');
    }

    public function getEsignById(Request $request, $id)
    {
        $esign = ESign::find($id);
        if ($esign) {

            return ResponseFormatter::success($esign, 'esign berhasil didapatakn');
        }
        return ResponseFormatter::error($esign, 'esign tidak ditemukan');
    }

    public function updateEsignById(Request $request, $id)
    {
        $request->validate([
            'judul' => ['required'],
            'id_penerima' => ['required'],
            'perihal' => ['required'],
            'waktu_valid' => ['required', 'date']
        ]);

        $esign = ESign::find($id);
        if (!$esign) {
            return ResponseFormatter::error($esign, 'e form tidak ditemukan');
        }

        $esign = ESign::find($id);
        $esign->update(
            [
                'judul' => $request->judul,
                'id_penerima' => $request->id_penerima,
                'perihal' => $request->perihal,
                'waktu_valid' => $request->waktu_valid,
                'catatan_tambahan' => $request->catatan_tambahan
            ]
        );
        return ResponseFormatter::success($esign, 'e form berhasil diupdate');
    }

    public function deleteEsignById(Request $request, $id)
    {
        $esign = ESign::find($id);
        if ($esign) {

            $esign->delete();
            return ResponseFormatter::success($esign, 'esign berhasil dihapus');
        }
        return ResponseFormatter::error($esign, 'esign tidak ditemukan');
    }

    public function acceptEsignById(Request $request, $id)
    {
        $request->validate([
            'catatan_tambahan' => ['required', 'string']
        ]);

        $sign = ESign::find($id)->update([
            'waktu_diterima' => new DateTime(),
            'catatan_tambahan' => $request->catatan_tambahan
        ]);

        return ResponseFormatter::success($sign, 'esign berhasil diterima');
    }

}
