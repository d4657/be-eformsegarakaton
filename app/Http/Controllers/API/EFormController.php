<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\ResponseFormatter;
use App\Models\EForm;
use App\Models\Surat;
use DateTime;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;

class EFormController extends Controller
{
    public function createEform(Request $request)
    {
        $request->validate([
            'judul' => ['required'],
            'id_penerima' => ['required'],
            'id_kategori' => ['required'],
            'lainnya' => ['required_if:id_kategori,0'],
            'dokumen' => ['required', 'file', 'mimes:doc,docx,pdf', 'max:2048'],
            'perihal' => ['required'],
            'keperluan_pribadi' => ['boolean'],
        ]);

        if ($request->id_kategori <= 0) {
            $lainnya = Surat::where('category', 'Lainnya')->first();
            $eform = EForm::create([
                'judul' => $request->judul,
                'id_pengirim' => Auth::user()->id,
                'id_penerima' => $request->id_penerima,
                'id_kategori' => $lainnya->id,
                'lainnya' => $request->lainnya,
                'perihal' => $request->perihal,
            ]);
        } else {

            if (!Surat::find($request->id_kategori)) {
                return ResponseFormatter::error(null, 'kategori dokumen invalid');
            }

            $eform = EForm::create([
                'judul' => $request->judul,
                'id_pengirim' => Auth::user()->id,
                'id_penerima' => $request->id_penerima,
                'id_kategori' => $request->id_kategori,
                'lainnya' => $request->lainnya,
                'perihal' => $request->perihal,
            ]);
        }

        $dokumen  = $request->dokumen->store('form-docs', 'public');
        $filename = pathinfo($dokumen, PATHINFO_BASENAME);
        $eform->dokumen = $filename;

        if ($request->keperluan_pribadi === true) {
            $eform->waktu_diterima = new DateTime();
            $eform->catatan_tambahan = $request->catatan_tambahan;
        }

        $eform->category;

        $eform->save();
        return ResponseFormatter::success($eform, 'e-sign berhasil diajukan');
    }

    public function getAllEform(Request $request)
    {
        $eform = EForm::all()
            ->each(
                function ($form) {
                    $form->category;
                }
            );

        return ResponseFormatter::success($eform, 'eform user aktif berhasil diambil');
    }

    public function getEformById(Request $request, $id)
    {
        $eform = EForm::find($id);
        $eform->category;
        return ResponseFormatter::success($eform);
    }

    public function updateEformById(Request $request, $id)
    {
        $request->validate([
            'judul' => ['required'],
            'id_penerima' => ['required'],
            'id_kategori' => ['required'],
            'lainnya' => ['required_if:id_kategori,0'],
            'perihal' => ['required'],
            'keperluan_pribadi' => ['boolean'],
        ]);

        $eform = EForm::find($id);
        if (!$eform) {
            return ResponseFormatter::error($eform, 'e form tidak ditemukan');
        }

        if ($request->id_kategori <= 0) {
            $lainnya = Surat::where('category', 'Lainnya')->first();
            $eform->update([
                'judul' => $request->judul,
                'id_penerima' => $request->id_penerima,
                'id_kategori' => $lainnya->id,
                'lainnya' => $request->lainnya,
                'perihal' => $request->perihal,
            ]);
        } else {
            $eform->update(
                [
                    'judul' => $request->judul,
                    'id_penerima' => $request->id_penerima,
                    'id_kategori' => $request->id_kategori,
                    'lainnya' => $request->lainnya,
                    'perihal' => $request->perihal,
                ]
            );
        }

        return ResponseFormatter::success($eform, 'e form berhasil diupdate');
    }

    public function updateEformDocById(Request $request, $id)
    {
        $eform = EForm::find($id);
        if (!$eform) {
            return ResponseFormatter::error(null, 'e form tidak ditemukan');
        }
        $request->validate([
            'dokumen' => ['required', 'file', 'mimes:docx,doc,pdf', 'max:2048']
        ]);

        if ($eform->dokumen) {
            try {
                unlink(public_path('storage/form-docs/' . $eform->dokumen));
            } catch (Exception $e) {
            }
        }

        $dokumen  = $request->dokumen->store('form-docs', 'public');
        $filename = pathinfo($dokumen, PATHINFO_BASENAME);
        $eform->dokumen = $filename;
        $eform->save();

        return ResponseFormatter::success($eform, 'dokumen eform berhasil diupdate');
    }

    public function deleteEformById(Request $request, $id)
    {
        $eform = EForm::find($id);
        if ($eform) {
            $eform->delete();
            return ResponseFormatter::success($eform);
        } else {
            return ResponseFormatter::error($eform, 'e form tidak ditemukan');
        }
    }

    public function acceptEformById(Request $request, $id)
    {
        $request->validate([
            'catatan_tambahan' => ['required', 'string']
        ]);

        try {
            $sign = EForm::find($id)->update([
                'waktu_diterima' => new DateTime(),
                'catatan_tambahan' => $request->catatan_tambahan
            ]);
        } catch (QueryException $e) {
            return ResponseFormatter::error($e->getMessage(), 'query error');
        }

        return ResponseFormatter::success($sign, 'esign berhasil diterima');
    }
}
