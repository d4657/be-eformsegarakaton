<?php

namespace App\Http\Controllers\API;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Rules\HpRule;
use App\Rules\NIKRule;
use App\Rules\PasswordRule;
use App\Rules\UsernameRule;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Str;
use PhpParser\Node\Stmt\TryCatch;

class UserController extends Controller
{
    //

    /**
     * user level :
     * -1 : head admin - hak akses admin + edit admin
     * 0  : admin - hak ases edior + edit user
     * 1  : editor - hak akses staff + edit surat
     * 2  : staff - ngurus permintaan tandatangan
     * 3  : penduduk - read only suratnya, eform dan etandatangan cuma bisa mengajukan permintaan
     */
    public function register(Request $request)
    {
        $request->validate(
            [
                'nama_lengkap' => ['required'],
                'username' => [new UsernameRule(), 'unique:users,username'],
                'email' => ['email', 'required', 'unique:users,email'],
                'password' => [new PasswordRule(), 'confirmed'],
                'password_confirmation' => ['required']
            ]
        );

        $user = User::create([
            'nama_lengkap' => $request->nama_lengkap,
            'username' => $request->username,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        return ResponseFormatter::success([
            'user' => $user
        ], 'User Created');
    }

    public function login(Request $request)
    {
        $request->validate([
            'username' => ['required'],
            'password' => ['required'],
        ]);

        $user = User::where('username', strtolower($request->username))->first();

        if (!$user) {
            return ResponseFormatter::error(['username' => ['user tidak ditemukan']]);
        }

        if (!$user || !Hash::check($request->password, $user->password)) {
            return ResponseFormatter::error(['username' => ['username dan password tidak sesuai']]);
        }

        if ($user->level_pengguna === 3) {
            $token = $user->createToken('authToken', ['role:penduduk'])->plainTextToken;
        } else if ($user->level_pengguna === 2) {
            $token = $user->createToken('authToken', ['role:staff'])->plainTextToken;
        } else if ($user->level_pengguna === 1) {
            $token = $user->createToken('authToken', ['role:editor'])->plainTextToken;
        } else if ($user->level_pengguna <= 0) {
            $token = $user->createToken('authToken', ['role:admin'])->plainTextToken;
        } else {
            return ResponseFormatter::error(
                ['user' => ['user tidak memiliki akses']]
            );
        }

        return ResponseFormatter::success([
            'user' => $user,
            'access_token' => $token,
        ]);
    }

    public function updateBioById(Request $request, $id)
    {
        $request->validate(
            [
                'nama_lengkap' => ['required', 'string'],
                'jabatan' => ['nullable', 'string'],
                'alamat'  => ['required', 'string'],
            ]
        );

        $user = User::find($id);
        if (!$user) {
            return ResponseFormatter::error(['user' => ['User tidak ditemukan']]);
        }

        $user->update(
            [
                'nama_lengkap' => $request->nama_lengkap,
                'jabatan' => $request->jabatan,
                'alamat' => $request->alamat,
            ]
        );

        return ResponseFormatter::success($user, ['Biodata user berhasil diupdate']);
    }

    public function  updateAccountById(Request $request, $id)
    {
        $request->validate(
            [
                'username' => [new UsernameRule(), 'unique:users,username,' . $id],
                'email' => ['email', 'required', 'unique:users,email,' . $id],
                'no_hp' => [new HpRule(), 'unique:users,no_hp,' . $id],
            ]
        );

        $user = User::find($id);
        if (!$user) {
            return ResponseFormatter::error(['user' => ['User tidak ditemukan']]);
        }

        $user->update(
            [
                'username' => $request->username,
                'email' => $request->email,
                'no_hp' => $request->no_hp,
            ]
        );

        return ResponseFormatter::success($user, 'detail akun user berhasil diupdate');
    }


    public function updatePasswordById(Request $request, $id)
    {
        $user = User::find($id);

        if (!$user) {
            return ResponseFormatter::error(['user' => ['User tidak ditemukan']]);
        }

        if (Auth::user()->level_pengguna === -1) {
            $request->validate([
                'password_baru' => ['required', new PasswordRule()],
                'password_baru_confirmation' => ['required'],
            ]);
        } else if (Auth::user()->level_pengguna === 0 && $user->level_pengguna > 0) {
            $request->validate([
                'password_baru' => ['required', new PasswordRule()],
                'password_baru_confirmation' => ['required'],
            ]);
        } else {
            $request->validate([
                'password_lama' => ['required'],
                'password_baru' => ['required', new PasswordRule()],
                'password_baru_confirmation' => ['required'],
            ]);

            if (!Hash::check($request->password_lama, $user->password)) {
                return ResponseFormatter::error(['password_lama' => ['Password tidak sesuai']]);
            }
        }

        $user->password = Hash::make($request->password_baru);
        $user->save();

        return ResponseFormatter::success($user, 'password berasil diupdate');
    }

    public function updateUserImagesById(Request $request, $id)
    {
        $request->validate(
            [
                'foto' => ['image', 'mimes:jpeg,png,jpg,gif,svg|max:2048'],
                'tandatangan' => ['image', 'mimes:jpeg,png,jpg,gif,svg|max:2048'],
                'x' => ['required'],
                'y' => ['required'],
                'width' => ['required'],
                'height' => ['required'],
            ]
        );

        $user = User::find($id);
        if (!$user) {
            return ResponseFormatter::error(['user' => ['User tidak ditemukan']]);
        }

        if ($request->foto) {
            if ($user->foto) {
                try {
                    unlink(public_path('storage/foto/' . $user->foto));
                } catch (Exception $e) {
                }
            }
            $foto  = $request->file('foto');
            $filename = Str::uuid()->toString() . '.png';
            Image::make($foto)
                ->crop($request->width, $request->height, $request->x, $request->y)
                ->resize(400, 400)
                ->save(public_path('storage/foto/' . $filename));
            $user->foto = $filename;
        }

        if ($request->tandatangan) {
            if ($user->tandatangan) {
                try {
                    unlink(public_path('storage/tandatangan/' . $user->tandatangan));
                } catch (Exception $e) {
                }
            }
            $tandatangan  = $request->file('tandatangan');
            $filename = Str::uuid()->toString() . '.png';
            Image::make($tandatangan)
                ->crop($request->width, $request->height, $request->x, $request->y)
                ->resize(600, 300)
                ->save(public_path('storage/tandatangan/' . $filename));
            $user->tandatangan = $filename;
        }

        $user->save();
        return ResponseFormatter::success($user, 'foto tandatangan berhasil diupdate');
    }


    public function updateAccount(Request $request)
    {
        $id = Auth::user()->id;
        return $this->updateAccountById($request, $id);
    }

    public function updateBio(Request $request)
    {
        $id = Auth::user()->id;
        return $this->updateBioById($request, $id);
    }

    public function updatePassword(Request $request)
    {
        $id = Auth::user()->id;
        return $this->updatePasswordById($request, $id);
    }

    public function updateUserImages(Request $request)
    {
        $id = Auth::user()->id;
        return $this->updateUserImagesById($request, $id);
    }

    public function deleteUserById(Request $request, $id)
    {
        $user = User::find($id);
        if (Auth::user()->level_pengguna < 0 || (Auth::user()->level_pengguna === 0 && $user->level_pengguna > 0)) {
            $user->delete();
            if ($user->foto) {
                if (file_exists(public_path('storage/foto/' . $user->foto))) {
                    try {
                        unlink(public_path('storage/foto/' . $user->foto));
                    } catch (\Exception $e) {
                    }
                }
            }
            if ($user->tandatangan) {
                if (file_exists(public_path('storage/tandatangan/' . $user->tandatangan))) {
                    try {
                        unlink(public_path('storage/tandatangan/' . $user->tandatangan));
                    } catch (\Exception $e) {
                    }
                }
            }
            return ResponseFormatter::success($user, 'user berhasil dihapus');
        }
        return ResponseFormatter::error('user gagal dihapus');
    }

    public function getUserById(Request $request, $id)
    {
        $user = User::find($id);
        return ResponseFormatter::success($user, 'user berhasil di request');
    }

    public function getAllUser(Request $request)
    {
        $user = User::all();
        return ResponseFormatter::success($user, 'user berhasil direquest');
    }

    public function getProfile(Request $request)
    {
        return ResponseFormatter::success(Auth::user(), 'profil berhasil direquest');
    }

    public function getAllAdmin(Request $request)
    {
        $select = array('id', 'nama_lengkap', 'foto', 'no_hp', 'alamat', 'jabatan');
        return ResponseFormatter::success(
            User::select($select)
                ->where('id', '!=', Auth::user()->id)
                ->where('level_pengguna', '<=', 0)
                ->where(
                    'nama_lengkap',
                    'like',
                    '%' . $request->cari_nama . '%'
                )
                ->paginate(
                    $request->perpage ? $request->perpage : 12,
                    ['*'],
                    'halaman',
                    $request->page ? $request->page : null,
                ),
            'admin berhasil diambil'
        );
    }
    public function getAllEditor(Request $request)
    {
        $select = array('id', 'nama_lengkap', 'foto', 'no_hp', 'alamat', 'jabatan');

        return ResponseFormatter::success(
            User::select($select)
                ->where('id', '!=', Auth::user()->id)
                ->where('level_pengguna', 1)
                ->where(
                    'nama_lengkap',
                    'like',
                    '%' . $request->cari_nama . '%'

                )
                ->paginate(
                    $request->perpage ? $request->perpage : 12,
                    ['*'],
                    'halaman',
                    $request->page ? $request->page : null,
                ),
            'editor berhasil diambil'
        );
    }
    public function getAllStaff(Request $request)
    {
        $select = array('id', 'nama_lengkap', 'foto', 'no_hp', 'alamat', 'jabatan');
        return ResponseFormatter::success(
            User::select($select)
                ->where('id', '!=', Auth::user()->id)
                ->where('level_pengguna', 2)
                ->where(
                    'nama_lengkap',
                    'like',
                    '%' . $request->cari_nama . '%'
                )
                ->paginate(
                    $request->perpage ? $request->perpage : 12,
                    ['*'],
                    'halaman',
                    $request->page ? $request->page : null,
                ),
            'staff berhasil diambil'
        );
    }

    public function getAllMasyarakat(Request $request)
    {
        $select = array('id', 'nama_lengkap', 'foto', 'no_hp', 'alamat', 'jabatan');

        return ResponseFormatter::success(
            User::select($select)
                ->where('id', '!=', Auth::user()->id)
                ->where('level_pengguna', 3)
                ->where(
                    'nama_lengkap',
                    'like',
                    '%' . $request->cari_nama . '%'
                )
                ->paginate(
                    $request->perpage ? $request->perpage : 12,
                    ['*'],
                    'halaman',
                    $request->page ? $request->page : null,
                ),
            "masyarakat sebanyak $request->perpage berhasil diambil di halaman $request->page"
        );
    }

    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();
        return ResponseFormatter::success('Token Revoked');
    }

    public function findStaffByName(Request $request)
    {
        $select = array('id', 'nama_lengkap');

        return ResponseFormatter::success(
            User::select($select)
                ->where('level_pengguna', '<=', 2)
                ->where(
                    'nama_lengkap',
                    'like',
                    '%' . $request->cari_nama . '%'
                )
                ->where('id', '!=', Auth::user()->id)
                ->whereNotNull('tandatangan')
                ->get(),

            'staff berhasil diambil'
        );
    }

    public function setUserLevel(Request $request, $id)
    {
        $request->validate(
            [
                'level', ['numeric', 'min:0', 'max:3']
            ]
        );
        $user = User::find($id);
        if (Auth::user()->level_pengguna < 0 || (Auth::user()->level_pengguna === 0 && $user->level_pengguna > 0)) {
            $user->level_pengguna = $request->level;
            $user->save();
            return ResponseFormatter::success($user, 'user berhasil diupdate');
        }
        return ResponseFormatter::error(['error' => ['hak akses user gagal diubah']]);
    }
}
