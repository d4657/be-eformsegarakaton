<?php

namespace App\Http\Controllers\API;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Models\ESign;
use App\Models\User;
use DateTime;
use Error;
use GrahamCampbell\ResultType\Success;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Support\Str;
use PhpParser\Node\Stmt\TryCatch;

class UsersEsignController extends Controller
{
    public function createUsersEsign(Request $request)
    {
        $request->validate([
            'judul' => ['required'],
            'id_penerima' => ['required_if:keperluan_pribadi,false'],
            'perihal' => ['required'],
            'keperluan_pribadi' => ['required', 'boolean'],
            'waktu_valid' => ['date']
        ]);

        $esign = null;

        if ($request->keperluan_pribadi == true) {
            $esign = ESign::create([
                'judul' => $request->judul,
                'id_pengirim' => Auth::user()->id,
                'id_penerima' => Auth::user()->id,
                'perihal' => $request->perihal,
                'waktu_valid' => $request->waktu_valid,
                'waktu_diterima' => new DateTime()
            ]);
            $this->generateQr($esign->id);
        } else {
            $receiver = User::find($request->id_penerima);

            if (!$receiver) {
                return ResponseFormatter::error(['id_penerima' => ['penerima e-sign tidak ditemukan']]);
            }

            if ($receiver->user_level > 2) {
                return ResponseFormatter::error(['id_penerima' => ['hanya staff dan admin yang bisa menerima permintaan e-sign']]);
            }

            $esign = ESign::create([
                'judul' => $request->judul,
                'id_pengirim' => Auth::user()->id,
                'id_penerima' => $request->id_penerima,
                'perihal' => $request->perihal,
                'waktu_valid' => new DateTime($request->waktu_valid . ' 00:00:00'),
            ]);
        }
        return ResponseFormatter::success($esign, 'e-form berhasil diajukan');
    }

    private function generateQr($id)
    {
        QrCode::size(300)
            ->format('png')
            ->generate(
                "https://eform-segarakaton.web.app/acc/$id",
                public_path('storage/esign-acc/' . $id . '.png')
            );
    }

    public function getAllUsersEsign(Request $request)
    {
        $user = Auth::user();
        return $this->getAllUsersEsignByUserId($request, $user->id);
    }

    private $user_id_selected;
    private $judul;
    public function getAllUsersEsignByUserId(Request $request, $user_id)
    {
        $this->user_id_selected = $user_id;
        $this->judul = $request->judul;
        $esign = ESign::where(function ($query) {
            $query->where('id_pengirim', $this->user_id_selected)
                ->orWhere('id_penerima', $this->user_id_selected);
        })
            ->where(
                function ($query) {
                    $query->where('judul', 'like', '%' . $this->judul . '%')
                        ->orWhere('perihal', 'like', '%' . $this->judul . '%');
                }
            )
            ->orderBy('created_at', $request->order ? $request->order : 'desc')
            ->paginate(
                $request->perpage ? $request->perpage : 5,
                ['*'],
                'halaman',
                $request->page ? $request->page : 1,
            );

        $esign->each(
            function ($dataEsign) {
                $dataEsign->pengirimSimple;
                $dataEsign->penerimaSimple;
            }
        );

        return ResponseFormatter::success($esign, 'esign user berhasil diambil');
    }

    public function getUsersEsignById(Request $request, $id)
    {
        $esign = ESign::find($id);

        if (!$esign) {
            return ResponseFormatter::error(['tidak ditemukan' => ['esign tidak ditemukan']]);
        }

        if ($esign->id_pengirim === Auth::user()->id || $esign->id_penerima === Auth::user()->id) {
            $esign->pengirimSimple;
            $esign->penerimaSimple;
            return ResponseFormatter::success($esign, 'esign user berhasil diambil');
        }
        return ResponseFormatter::error(['esign' => ['bukan merupakaan milik user ']]);
    }

    public function updateUserEsignById(Request $request, $id)
    {
        $request->validate([
            'judul' => ['required'],
            'id_penerima' => ['required'],
            'perihal' => ['required'],
            'waktu_valid' => ['required', 'date']
        ]);

        $user = Auth::user();
        $esign = $user->esignSent->find($id);

        if (!$esign) {
            return ResponseFormatter::error($esign, 'e-sign tidak ditemukan');
        }

        if ($esign->waktu_diterima) {
            return ResponseFormatter::error($esign, 'e-sign yang sudah di-acc tidak bisa edit kembali');
        }

        $esign->update(
            [
                'judul' => $request->judul,
                'id_penerima' => $request->id_penerima,
                'perihal' => $request->perihal,
                'waktu_valid' => $request->waktu_valid,
                'catatan_tambahan' => $request->catatan_tambahan
            ]
        );
        $esign = ESign::find($id);
        $esign->pengirimSimple;
        $esign->penerimaSimple;
        return ResponseFormatter::success($esign, 'e-esign berhasil diupdate');
    }

    public function acceptEsign(Request $request, $id)
    {
        $request->validate([
            'catatan_tambahan' => ['required', 'string']
        ]);
        $user = Auth::user();
        $sign = $user->esignReceived->find($id);

        if (!$sign) {
            return ResponseFormatter::error(['catatan_tambahan' => ['e-sign bukan milik pengguna yang sesuai']]);
        }

        if (!$sign->waktu_diterima) {
            $sign->update([
                'waktu_diterima' => new DateTime(),
                'catatan_tambahan' => $request->catatan_tambahan
            ]);
        } else {
            return ResponseFormatter::error(['catatan_tambahan' => ['e-sign sebelumnya telah di-acc']]);
        }

        $this->generateQr($id);
        $esign = ESign::find($id);
        $esign->pengirimSimple;
        $esign->penerimaSimple;
        return ResponseFormatter::success($esign, 'e-sign berhasil diterima');
    }

    public function delUsersEsignById($id)
    {
        $esign = Auth::user()->esignSent;
        $esign  = $esign->find($id);

        if (!$esign) {
            return ResponseFormatter::error('esign tidak ditemukan');
        }

        if ($esign->waktu_diterima && $esign->id_pengirim !== $esign->id_penerima) {
            return ResponseFormatter::error('e-sign tidak bisa dihapus karena telah di-acc');
        }

        if (file_exists(public_path('storage/esign-acc/' . $id . '.png'))) {
            try {
                unlink(public_path('storage/esign-acc/' . $id . '.png'));
            } catch (\Exception $e) {
            }
        }

        $esign->delete();
        return ResponseFormatter::success($esign, 'esign berhasil dihapus');
    }

    public function getAcceptedEsign($id)
    {
        $esign = Esign::whereNotNull('waktu_diterima')->find($id);
        if (!$esign) {
            return ResponseFormatter::error(['esign' => ['e-sign belum diacc atau tidak ditemukan']]);
        }
        $esign->penerimaSimple;
        $esign->pengirimSimple;
        return ResponseFormatter::success($esign, 'esign berhasil diambil');
    }
}
