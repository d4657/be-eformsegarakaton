<?php

namespace App\Http\Controllers\API;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Models\Surat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SuratController extends Controller
{
    //
    public function createSurat(Request $request)
    {
        $request->validate(
            [
                'jenis_surat' => ['required', 'string'],
                'deskripsi' => ['required', 'string'],
                'link_dokumen' => ['required', 'active_url'],
                'template' => ['required', 'mimes:doc,docx,pdf', 'max:2048']
            ]
        );

        $surat = Surat::create(
            [
                'jenis_surat' => $request->jenis_surat,
                'deskripsi' => $request->deskripsi,
                'link_dokumen' => $request->link_dokumen,
            ]
        );

        if ($request->template) {
            $template  = $request->template->store('template', 'public');
            $filename = pathinfo($template, PATHINFO_BASENAME);
            $surat->template = $filename;
        }

        $surat->save();

        return ResponseFormatter::success($surat);
    }

    public function getSurat(Request $request)
    {
        $surats = Surat::where(
            'jenis_surat',
            'like',
            "%$request->cari%"
        )
            ->orWhere(
                'deskripsi',
                'like',
                "%$request->cari%"
            )
            ->paginate(
                $request->perpage ? $request->perpage : 12,
                ['*'],
                'halaman',
                $request->page ? $request->page : null,
            );
        return ResponseFormatter::success($surats, 'surat berhasil diambil');
    }

    public function getSuratById(Request $request, $id)
    {
        return ResponseFormatter::success(Surat::find($id), 'surat berhasil diambil');
    }

    public function updateSuratById(Request $request, $id)
    {


        $request->validate(
            [
                'jenis_surat' => ['required'],
                'deskripsi' => ['required'],
                'link_dokumen' => ['required'],
                'template' => ['nullable', 'file', 'mimes:doc,docx,pdf', 'max:2048',]
            ]
        );


        $surat = Surat::find($id);

        if ($request->template) {
            if ($surat->template) {
                if (file_exists(public_path('storage/template/' . $surat->template))) {
                    unlink(public_path('storage/template/' . $surat->template));
                }
            }

            $template  = $request->template->store('template', 'public');
            $filename = pathinfo($template, PATHINFO_BASENAME);
            $surat->template = $filename;
            $surat->save();
        }

        $surat->update(
            [
                'jenis_surat' => $request->jenis_surat,
                'deskripsi' => $request->deskripsi,
                'link_dokumen' => $request->link_dokumen,
            ]
        );


        return ResponseFormatter::success($surat);
    }

    public function updateTemplateById(Request $request, $id)
    {
        $request->validate([
            'template' => ['file', 'mimes:doc,docx,pdf', 'max:2048']
        ]);

        $surat = Surat::find($id);

        if ($request->template) {
            if ($surat->template) {
                if (file_exists(public_path('storage/template/' . $surat->template))) {
                    unlink(public_path('storage/template/' . $surat->template));
                }
            }

            $template  = $request->template->store('template', 'public');
            $filename = pathinfo($template, PATHINFO_BASENAME);
            $surat->template = $filename;
        }

        $surat->save();
        return ResponseFormatter::success($surat, 'Template surat berhasil diupdate');
    }

    public function deleteSuratById(Request $request, $id)
    {
        $surat = Surat::find($id);
        if (!$surat) {
            return ResponseFormatter::error(
                ['surat' => ['surat tidak memiliki akses']]
            );
        }

        if ($surat->template) {
            if (file_exists(public_path('storage/template/' . $surat->template))) {
                try {
                    unlink(public_path('storage/template/' . $surat->template));
                } catch (\Exception $e) {
                }
            }
        }

        if (Auth::user()->level_pengguna < 1) {
            $surat->delete();
            return ResponseFormatter::success($surat, 'surat berhasil dihapus');
        }

        return ResponseFormatter::error(
            ['surat' => ['user tidak memiliki akses']]
        );
    }

    public function batchDeleteSurat(Request $request)
    {
        $request->validate(
            [
                'ids' => 'required'
            ]
        );

        $surat = Surat::whereIn('id', $request->ids)->delete();

        return ResponseFormatter::success($surat, 'surat berhasil dihapus');
    }
}
