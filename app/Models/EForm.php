<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EForm extends Model
{
    use Uuids;
    use HasFactory;

    protected $fillable = [
        'judul',
        'id_pengirim',
        'id_penerima',
        'id_kategori',
        'lainnya',
        'dokumen',
        'perihal',
        'waktu_valid',
        'waktu_diterima',
        'catatan_tambahan',
    ];

    public function category(){
        return $this->belongsTo(Surat::class, 'id_kategori');
    }

}
