<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ESign extends Model
{
    use Uuids;
    use HasFactory;

    protected $fillable = [
        'judul',
        'id_pengirim',
        'id_penerima',
        'perihal',
        'waktu_valid',
        'waktu_diterima',
        'catatan_tambahan'
    ];

    public function pengirim()
    {
        return $this->belongsTo(User::class, 'id_pengirim');
    }

    public function penerima()
    {
        return $this->belongsTo(User::class, 'id_penerima');
    }

    public function pengirimSimple()
    {
        return $this->belongsTo(User::class, 'id_pengirim')->select(array('id', 'nama_lengkap', 'foto'));
    }

    public function penerimaSimple()
    {
        return $this->belongsTo(User::class, 'id_penerima')->select(array('id', 'nama_lengkap', 'foto'));
    }
}
