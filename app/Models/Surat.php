<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Surat extends Model
{
    use Uuids;
    use HasFactory;

    protected $fillable = [
        'jenis_surat',
        'deskripsi',
        'link_dokumen',
        'template',
    ];

    public function form()
    {
        return $this->hasMany(EForm::class, 'id_kategori');
    }
}
