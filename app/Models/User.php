<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use Uuids;
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'nama_lengkap',
        'jabatan',
        'password',
        'username',
        'email',
        'alamat',
        'no_hp',
        'foto',
        'tandatangan',
        'level_pengguna',
        /**
         * user level :
         * -1 : head admin - hak akses admin + edit admin
         * 0  : admin - hak ases edior + edit user
         * 1  : editor - hak akses staff + edit surat
         * 2  : staff - ngurus permintaan tandatangan
         * 3  : penduduk - read only suratnya, eform dan esign cuma bisa mengajukan permintaan
         */
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function esignSent()
    {
        return $this->hasMany(ESign::class, 'id_pengirim');
    }

    public function esignReceived()
    {
        return $this->hasMany(ESign::class, 'id_penerima');
    }

    public function eformSent()
    {
        return $this->hasMany(EForm::class, 'id_pegirim');
    }

    public function eformReceived()
    {
        return $this->hasMany(EForm::class, 'id_penerima');
    }
}
