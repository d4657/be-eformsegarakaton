<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory()->create([
            'nama_lengkap' => 'admin',
            'username' => 'admin202',
            'email' => 'admin@gmail.com',
            'no_hp' => '081234567890',
            'password' => '$2y$10$l7kcMmXODClylWtC9D2qHOCuJ/tU.JXI6t5PXe../c/.F5TuvBmru',
            'level_pengguna' => -1,
        ]);
    }
}
