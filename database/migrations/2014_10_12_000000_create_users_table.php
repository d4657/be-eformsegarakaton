<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('nama_lengkap');
            $table->string('jabatan')->nullable();
            $table->text('alamat')->nullable();

            $table->string('password');

            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->string('no_hp')->unique()->nullable();

            $table->string('foto')->nullable();
            $table->string('tandatangan')->nullable();

            $table->integer('level_pengguna')->default(3);

            $table->rememberToken();
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
