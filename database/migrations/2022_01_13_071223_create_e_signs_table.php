<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;

class CreateESignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('e_signs', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->timestamps();
            $table->string('judul');

            $table->uuid('id_pengirim');
            $table->foreign('id_pengirim')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->uuid('id_penerima');
            $table->foreign('id_penerima')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->text('perihal');
            $table->timestamp('waktu_valid')->useCurrent();
            $table->timestamp('waktu_diterima')->nullable();
            $table->text('catatan_tambahan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('e_signs');
    }
}
